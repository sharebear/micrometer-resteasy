package com.gitlab.sharebear.micrometer.resteasy;

import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import javax.ws.rs.Path;
import javax.ws.rs.container.DynamicFeature;
import javax.ws.rs.container.ResourceInfo;
import javax.ws.rs.core.FeatureContext;

public class MicrometerFeature implements DynamicFeature {

  private final Map<String, TimerInterceptor> interceptorMap = new ConcurrentHashMap<>();

  @Override
  public void configure(ResourceInfo resourceInfo, FeatureContext context) {
    final String path = getPath(resourceInfo);
    if (!interceptorMap.containsKey(path)) {
      interceptorMap.put(path, new TimerInterceptor(path));
    }
    context.register(interceptorMap.get(path));
  }

  /**
   * Caveat, this implementation is incorrect if; * the resource is a correctly nested JAX-RS
   * resource * the @Application has a path * the Servlet is mounted on a path other than /
   *
   * <p>However, it's good enough for my current usage.
   */
  private String getPath(ResourceInfo resourceInfo) {
    final String pathFromClassAnnotation =
        getPathSection(resourceInfo.getResourceClass().getAnnotation(Path.class));
    final String pathFromMethodAnnotation =
        getPathSection(resourceInfo.getResourceMethod().getAnnotation(Path.class));
    return pathFromClassAnnotation + pathFromMethodAnnotation;
  }

  private static String getPathSection(Path annotation) {
    return Optional.ofNullable(annotation)
        .map(Path::value)
        .map(MicrometerFeature::sanitisePathSegment)
        .orElse("");
  }

  private static String sanitisePathSegment(String s) {
    return ensureSlashAtStart(removeSlashFromEnd(s));
  }

  private static String removeSlashFromEnd(String s) {
    if (s.endsWith("/")) {
      return s.substring(0, s.length() - 1);
    }
    return s;
  }

  private static String ensureSlashAtStart(String s) {
    if (s.startsWith("/")) {
      return s;
    }
    return "/" + s;
  }
}
