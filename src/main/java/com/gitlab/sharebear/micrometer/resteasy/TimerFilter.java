package com.gitlab.sharebear.micrometer.resteasy;

import static java.util.Arrays.asList;

import io.micrometer.core.instrument.*;
import java.io.IOException;
import java.util.List;
import java.util.Optional;
import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class TimerFilter implements Filter {

  private final MeterRegistry registry;

  @SuppressWarnings("unused")
  public TimerFilter() {
    registry = Metrics.globalRegistry;
  }

  @SuppressWarnings("WeakerAccess")
  public TimerFilter(MeterRegistry registry) {
    this.registry = registry;
  }

  @Override
  public void init(FilterConfig filterConfig) {}

  @Override
  public void doFilter(
      ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain)
      throws IOException, ServletException {
    final Timer.Sample sample = Timer.start(registry);
    HttpServletResponse httpResponse = (HttpServletResponse) servletResponse;
    HttpServletRequest httpRequest = (HttpServletRequest) servletRequest;

    try {
      filterChain.doFilter(servletRequest, servletResponse);
      recordTimer(sample, httpRequest, httpResponse);
    } catch (Exception e) {
      recordTimer(sample, httpRequest, e);
      throw e;
    }
  }

  private void recordTimer(
      Timer.Sample sample, HttpServletRequest httpRequest, HttpServletResponse httpResponse) {
    final List<Tag> tags =
        asList(
            Tag.of("response", Integer.toString(httpResponse.getStatus())),
            Tag.of("method", httpRequest.getMethod()),
            Tag.of("path", getPathFromRequestIfExists(httpRequest)));

    sample.stop(registry.timer("http.server.requests", tags));
  }

  private void recordTimer(Timer.Sample sample, HttpServletRequest httpRequest, Exception e) {
    final List<Tag> tags =
        asList(
            Tag.of("response", "500"),
            Tag.of("method", httpRequest.getMethod()),
            Tag.of("path", getPathFromRequestIfExists(httpRequest)),
            Tag.of("exception", getExceptionName(e)));

    sample.stop(registry.timer("http.server.requests", tags));
  }

  private static String getPathFromRequestIfExists(HttpServletRequest httpRequest) {
    return Optional.ofNullable(httpRequest.getAttribute("jax-rs.path"))
        .map(attribute -> (String) attribute)
        .orElse("NON-JAX-RS-RESOURCE");
  }

  private static String getExceptionName(Exception e) {
    return Optional.ofNullable(e.getCause()).orElse(e).getClass().getSimpleName();
  }

  @Override
  public void destroy() {}
}
