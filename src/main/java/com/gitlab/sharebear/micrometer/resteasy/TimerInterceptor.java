package com.gitlab.sharebear.micrometer.resteasy;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.Context;
import javax.ws.rs.ext.Provider;

@Provider
public class TimerInterceptor implements ContainerRequestFilter {

  private final String path;

  @Context private HttpServletRequest httpRequest;

  TimerInterceptor(String path) {
    this.path = path;
  }

  @Override
  public void filter(ContainerRequestContext containerRequestContext) {
    httpRequest.setAttribute("jax-rs.path", path);
  }
}
