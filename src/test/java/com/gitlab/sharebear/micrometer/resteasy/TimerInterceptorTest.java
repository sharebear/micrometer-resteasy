package com.gitlab.sharebear.micrometer.resteasy;

import static org.assertj.core.api.Assertions.assertThat;

import io.micrometer.core.instrument.Meter;
import io.micrometer.core.instrument.MeterRegistry;
import io.micrometer.core.instrument.simple.SimpleMeterRegistry;
import io.undertow.servlet.Servlets;
import io.undertow.servlet.util.ImmediateInstanceFactory;
import java.io.IOException;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.servlet.DispatcherType;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Application;
import okhttp3.*;
import org.jboss.resteasy.plugins.server.undertow.UndertowJaxrsServer;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class TimerInterceptorTest {
  private MeterRegistry registry;
  private UndertowJaxrsServer server;
  private final OkHttpClient client = new OkHttpClient();

  public static class TestApp extends Application {

    @Override
    public Set<Class<?>> getClasses() {
      return Stream.of(TestResource.class).collect(Collectors.toSet());
    }

    @Override
    public Set<Object> getSingletons() {
      return Stream.of(new MicrometerFeature()).collect(Collectors.toSet());
    }
  }

  @Path("/rootResource")
  public static class TestResource {
    @Path("/hello")
    @GET
    public String hello() {
      return "Hello world";
    }

    @Path("/pathWithSlashAtEnd/")
    @GET
    public String pathWithSlashAtEnd() {
      return "Hello world";
    }

    @Path("/hello")
    @POST
    public String helloPost() {
      return "Hello world";
    }

    @Path("/hello/{subject}")
    @GET
    public String helloPersonal(@PathParam("subject") String subject) {
      return String.format("Hello %s", subject);
    }

    @Path("/boom")
    @GET
    public String goBoom() {
      throw new UnhandledBusinessException();
    }
  }

  private static class UnhandledBusinessException extends RuntimeException {}

  @Before
  public void startServer() {
    registry = new SimpleMeterRegistry();
    final TimerFilter timerFilter = new TimerFilter(registry);

    server = new UndertowJaxrsServer().start();
    server.deploy(
        server
            .undertowDeployment(TestApp.class)
            .setDeploymentName("TestApp")
            .setContextPath("/")
            .addFilter(
                Servlets.filter(
                    "TimerFilter", TimerFilter.class, new ImmediateInstanceFactory<>(timerFilter)))
            .addFilterUrlMapping("TimerFilter", "/*", DispatcherType.REQUEST));
  }

  @Test
  public void shouldSetMethodTagToGETForGETRequest() throws IOException {
    final Request request =
        new Request.Builder().url("http://localhost:8081/rootResource/hello").build();

    try (Response response = client.newCall(request).execute()) {
      assertThat(response.code()).isEqualTo(200);
      assertThat(response.body().string()).isEqualTo("Hello world");
    }

    final List<Meter> meters = registry.getMeters();
    assertThat(meters).hasSize(1);

    final Meter meter = meters.get(0);
    final Meter.Id meterId = meter.getId();
    assertThat(meterId.getTag("method")).isEqualTo("GET");
    assertThat(meterId.getTag("path")).isEqualTo("/rootResource/hello");
  }

  @Test
  public void shouldRemoveTrailingSlashFromPath() throws IOException {
    final Request request =
        new Request.Builder().url("http://localhost:8081/rootResource/pathWithSlashAtEnd/").build();

    try (Response response = client.newCall(request).execute()) {
      assertThat(response.code()).isEqualTo(200);
      assertThat(response.body().string()).isEqualTo("Hello world");
    }

    final List<Meter> meters = registry.getMeters();
    assertThat(meters).hasSize(1);

    final Meter meter = meters.get(0);
    final Meter.Id meterId = meter.getId();
    assertThat(meterId.getTag("method")).isEqualTo("GET");
    assertThat(meterId.getTag("path")).isEqualTo("/rootResource/pathWithSlashAtEnd");
  }

  @Test
  public void shouldSetMethodTagToPOSTForPOSTRequest() throws IOException {
    final Request request =
        new Request.Builder()
            .url("http://localhost:8081/rootResource/hello")
            .post(RequestBody.create(MediaType.parse("text/plain"), "DummyContent"))
            .build();

    try (Response response = client.newCall(request).execute()) {
      assertThat(response.code()).isEqualTo(200);
      assertThat(response.body().string()).isEqualTo("Hello world");
    }

    final List<Meter> meters = registry.getMeters();
    assertThat(meters).hasSize(1);

    final Meter meter = meters.get(0);
    final Meter.Id meterId = meter.getId();
    assertThat(meterId.getTag("method")).isEqualTo("POST");
  }

  @Test
  public void shouldMaintainParametersInPath() throws IOException {
    final Request request =
        new Request.Builder().url("http://localhost:8081/rootResource/hello/micrometer").build();

    try (Response response = client.newCall(request).execute()) {
      assertThat(response.code()).isEqualTo(200);
      assertThat(response.body().string()).isEqualTo("Hello micrometer");
    }

    final List<Meter> meters = registry.getMeters();
    assertThat(meters).hasSize(1);

    final Meter meter = meters.get(0);
    final Meter.Id meterId = meter.getId();
    assertThat(meterId.getTag("path")).isEqualTo("/rootResource/hello/{subject}");
  }

  @Test
  public void shouldRecord500StatusAndExceptionNameForUnhandledException() throws IOException {
    final Request request =
        new Request.Builder().url("http://localhost:8081/rootResource/boom").build();

    try (Response response = client.newCall(request).execute()) {
      assertThat(response.code()).isEqualTo(500);
    }

    final List<Meter> meters = registry.getMeters();
    assertThat(meters).hasSize(1);

    final Meter meter = meters.get(0);
    final Meter.Id meterId = meter.getId();
    assertThat(meterId.getTag("response")).isEqualTo("500");
    assertThat(meterId.getTag("exception")).isEqualTo("UnhandledBusinessException");
  }

  @Test
  public void shouldRecord404StatusWhenNoMatchingJaxRsResource() throws IOException {
    final Request request =
        new Request.Builder().url("http://localhost:8081/rootResource/404").build();

    try (Response response = client.newCall(request).execute()) {
      assertThat(response.code()).isEqualTo(404);
    }

    final List<Meter> meters = registry.getMeters();
    assertThat(meters).hasSize(1);

    final Meter meter = meters.get(0);
    final Meter.Id meterId = meter.getId();
    assertThat(meterId.getTag("response")).isEqualTo("404");
    assertThat(meterId.getTag("path")).isEqualTo("NON-JAX-RS-RESOURCE");
  }

  @After
  public void stopServer() {
    registry.close();
    // WARNING: This is asynch, mulitple runs should be guarded to ensure first server has stopped
    server.stop();
  }
}
